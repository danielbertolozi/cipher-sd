const CliInterface = require("./dist/main/cli-interface/CLIInterface").CLIInterface;
const ArgumentHandler = require("./dist/main/cli-interface/CommanderArgumentHandler").CommanderArgumentHandler;

const cli = new CliInterface(new ArgumentHandler());
cli.main(process.argv);