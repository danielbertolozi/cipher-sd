# cipher-sd

Search and Destroy Cipher algorithm implemented in Node.js.

This will try to run all implemented Ciphers over a given string, and present the decoded result.

## How to run

`node index.js "phrase between quotation marks"`

## Env Setup

* `git clone https://gitlab.com/danielbertolozi/cipher-sd.git`
* `npm i`

Unit tests can be executed with `npm test`.

The roadmap can be kind of seen in the wiki.

## Contribution

Contribution is always welcome. Feel free to fork the repo and add your own implementations.

Be sure to follow what have been done so far - unit tests for everything (the `CipherTester` makes it very easy to write tests here), follow the class naming and extension conventions, etc.