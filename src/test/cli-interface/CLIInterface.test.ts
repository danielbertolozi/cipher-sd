import { ArgumentHandler } from "../../main/cli-interface/ArgumentHandler";
import { CLIInterface } from "../../main/cli-interface/CLIInterface";

class MockHandler implements ArgumentHandler {
    public called: boolean = false;
    
    callUnderlyingHandler(args: string[]): void {
        this.called = true;
    }
}

context("CLI Interface", () => {
    describe("when calling function main", () => {
        it("should pass the arguments to the handler class", () => {
            const cliInterface = new CLIInterface(new MockHandler());
            const args = ["sample"];
            cliInterface.main(args);
        });
    });
});