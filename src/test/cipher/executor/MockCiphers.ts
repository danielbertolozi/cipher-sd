import { Cipher } from "../../../main/cipher/Cipher";

export class MockCipher implements Cipher {
    public called: boolean = false;
    
    checkIfAppliable(input: string): boolean {
        return true;
    }
    
    apply(input: string): string {
        this.called = true;
        return "MockCipher called";
    }
}

export class MockNonCalledCipher implements Cipher {
    public called: boolean = false;
    
    checkIfAppliable(input: string): boolean {
        return false;
    }
    
    apply(input: string): string {
        this.called = true;
        return "MockCipher called";
    }
}