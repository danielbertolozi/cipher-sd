import { Cipher } from "../../../main/cipher/Cipher";
import { EligibilityChecker } from "../../../main/cipher/executor/EligibilityChecker";
import { expect } from "chai";
import { MockCipher, MockNonCalledCipher } from "./MockCiphers";

let MockCipherInstanceExporter: Cipher[];

context("Eligibility Checker", () => {
    context("on calling function getEligibleCiphers", () => {
        beforeEach(() => {
            MockCipherInstanceExporter = [];
        });
        it("should return all ciphers where identify returned true", () => {
            MockCipherInstanceExporter.push(new MockCipher());
            const eligibilityChecker = new EligibilityChecker(MockCipherInstanceExporter);
            const result = eligibilityChecker.getEligibleCiphers("");

            expect(result.length).to.be.equal(1);
        });
        it("should not add ciphers that are not eligible", () => {
            MockCipherInstanceExporter.push(new MockNonCalledCipher());
            const eligibilityChecker = new EligibilityChecker(MockCipherInstanceExporter);
            const result = eligibilityChecker.getEligibleCiphers("");

            expect(result.length).to.be.equal(0);
        });
        it("should return an empty array if there no ciphers on the provided Instance Exporter", () => {
            const eligibilityChecker = new EligibilityChecker(MockCipherInstanceExporter);
            const result = eligibilityChecker.getEligibleCiphers("");

            expect(result.length).to.be.equal(0);
        });
    });
});