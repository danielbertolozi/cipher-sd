import { CipherApplier } from "../../../main/cipher/executor/CipherApplier";
import { Cipher } from "../../../main/cipher/Cipher";
import { MockCipher } from "./MockCiphers";
import { expect } from "chai";
import { CipherResponseHandler } from "../../../main/cipher/executor/responseHandlers/CipherResponseHandler";

class MockResponseHandler implements CipherResponseHandler {
    public called: boolean = false;
    
    forward(response: string): void {
        this.called = true;
    }
}

let MockCipherProvider: Cipher[];
context("Cipher Applier", () => {
    beforeEach(() => {
        MockCipherProvider = [];
    });
    describe("when calling function applyCiphers", () => {
        it("should call function apply from all Ciphers that were provided", () => {
            const mockCipher = new MockCipher();
            MockCipherProvider.push(mockCipher);
            const cipherApplier = new CipherApplier(MockCipherProvider);
            const handler = new MockResponseHandler();
            cipherApplier.setResponseHandler(handler);
            
            cipherApplier.apply("");
            expect(mockCipher.called).to.be.true;
        });
        it("should call function forward from the CipherResponseHandler, if it has been set", () => {
            MockCipherProvider.push(new MockCipher());
            const cipherApplier = new CipherApplier(MockCipherProvider);
            const handler = new MockResponseHandler();
            cipherApplier.setResponseHandler(handler);
            
            cipherApplier.apply("");
            expect(handler.called).to.be.true;
        });
        it("should throw an error if the CipherResponseHandler has not been set", () => {
            const cipherApplier = new CipherApplier(MockCipherProvider);
            
            expect(() => cipherApplier.apply("")).to.throw;
        });
    });
});