import { BaseCipher } from "../../../main/cipher/implementations/BaseCipher";
import { expect } from "chai";

export class CipherTester {
    public static testIdentify(instance: BaseCipher, input: string): void {
        it(`should return true if the input is a ${instance.constructor.name} cipher`, () => {
            expect(instance.checkIfAppliable(input)).to.be.true;
        });
        
    }

    public static testIdentify_Negative(instance: BaseCipher, input: string): void {
        it(`should return false if the input is a ${instance.constructor.name} cipher`, () => {
            expect(instance.checkIfAppliable(input)).to.be.false;
        });
    }

    public static testApply(instance: BaseCipher, input: string, expected: string): void {
        it("should decrypt the string correctly", () => {
            expect(instance.apply(input)).to.be.equal(expected);
        });
    }

    public static testApply_MultipleOutputs(instance: BaseCipher, input: string, expected: string): void {
        it("should output all possible options for that string, and contain the expected", () => {
            expect(instance.apply(input)).to.contain(expected);
        });
    }
}