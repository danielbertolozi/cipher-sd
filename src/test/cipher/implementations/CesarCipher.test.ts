import { CesarCipher } from "../../../main/cipher/implementations/CesarCipher";
import { CipherTester } from "./CipherTester";

let cipher: CesarCipher;
context("Cesar Cipher", () => {
    describe("when calling function checkIfAppliable", () => {
        const falseTest = "aabb";
        const trueTest = "asdasdasd";

        cipher = new CesarCipher();
        CipherTester.testIdentify(cipher, trueTest);
        CipherTester.testIdentify_Negative(cipher, falseTest);
    });
    describe("when calling method apply", () => {
        const unencryptedPhrase = "THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG";
        const encryptedPhrase = "AOL XBPJR IYVDU MVE QBTWZ VCLY AOL SHGF KVN";

        cipher = new CesarCipher();
        CipherTester.testApply_MultipleOutputs(cipher, encryptedPhrase, unencryptedPhrase);
    });
});
