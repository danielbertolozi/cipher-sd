import { B64 } from "../../../main/cipher/implementations/B64";
import { CipherTester } from "./CipherTester";

let cipher: B64;
context("B64", () => {
    describe("when calling function checkIfAppliable", () => {
        const falseTest = "baaab baaba baaaa abaaa abaab aabaa abbaa abbab babaa";
        const trueTest = "TWFuIGlzIGRpc3Rpbmd1aXNoZWQsIG5vdCBvbmx5IGJ5IGhpcyByZWFzb24sIGJ1dCBieSB0aGlzIHNpbmd1bGFyIHBhc3Npb24gZnJvbSBvdGhlciBhbmltYWxzLCB3aGljaCBpcyBhIGx1c3Qgb2YgdGhlIG1pbmQsIHRoYXQgYnkgYSBwZXJzZXZlcmFuY2Ugb2YgZGVsaWdodCBpbiB0aGUgY29udGludWVkIGFuZCBpbmRlZmF0aWdhYmxlIGdlbmVyYXRpb24gb2Yga25vd2xlZGdlLCBleGNlZWRzIHRoZSBzaG9ydCB2ZWhlbWVuY2Ugb2YgYW55IGNhcm5hbCBwbGVhc3VyZS4=";

        cipher = new B64();
        CipherTester.testIdentify(cipher, trueTest);
        CipherTester.testIdentify_Negative(cipher, falseTest);
    });
    describe("when calling method apply", () => {
        const unencryptedPhrase = "The quick brown fox jumps over the lazy dog";
        const encryptedPhrase = "VGhlIHF1aWNrIGJyb3duIGZveCBqdW1wcyBvdmVyIHRoZSBsYXp5IGRvZw==";

        cipher = new B64();
        CipherTester.testApply(cipher, encryptedPhrase, unencryptedPhrase);
    });
});
