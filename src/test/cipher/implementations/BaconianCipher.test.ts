import { BaconianCipher } from "../../../main/cipher/implementations/BaconianCipher";
import { CipherTester } from "./CipherTester";

let cipher: BaconianCipher;
context("Baconian Cipher", () => {
    describe("when calling function checkIfAppliable", () => {
        const falseTest = "TWFuIGlzIGRpc3Rpbmd1aXNoZWQsIG5vdCBvbmx5IGJ5IGhpcyByZWFzb24sIGJ1dCBieSB0aGlzIHNpbmd1bGFyIHBhc3Npb24gZnJvbSBvdGhlciBhbmltYWxzLCB3aGljaCBpcyBhIGx1c3Qgb2YgdGhlIG1pbmQsIHRoYXQgYnkgYSBwZXJzZXZlcmFuY2Ugb2YgZGVsaWdodCBpbiB0aGUgY29udGludWVkIGFuZCBpbmRlZmF0aWdhYmxlIGdlbmVyYXRpb24gb2Yga25vd2xlZGdlLCBleGNlZWRzIHRoZSBzaG9ydCB2ZWhlbWVuY2Ugb2YgYW55IGNhcm5hbCBwbGVhc3VyZS4=";
        const trueTest = "baaab baaba baaaa abaaa abaab aabaa abbaa abbab babaa";

        cipher = new BaconianCipher();
        CipherTester.testIdentify(cipher, trueTest);
        CipherTester.testIdentify_Negative(cipher, falseTest);
    });
    describe("when calling method apply", () => {
        const unencryptedPhrase = "THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG";
        const encryptedPhrase = "BAABBAABBBAABAA BAAAABABAAABAAAAAABAABABA AAAABBAAABABBBABABBAABBAB AABABABBBABABBB ABAABBABAAABBAAABBBBBAABA ABBBABABABAABAABAAAB BAABBAABBBAABAA ABABBAAAAABBAABBBAAA AAABBABBBAAABBA";

        cipher = new BaconianCipher();
        CipherTester.testApply(cipher, encryptedPhrase, unencryptedPhrase);
    });
});
