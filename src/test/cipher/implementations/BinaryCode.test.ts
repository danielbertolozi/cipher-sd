import { BinaryCode } from "../../../main/cipher/implementations/BinaryCode";
import { CipherTester } from "./CipherTester";

let cipher: BinaryCode;
context("Binary Code", () => {
    describe("when calling function checkIfAppliable", () => {
        const falseTest = "TWFuIGlzIGRpc3Rpbmd1aXNoZWQsIG5vdCBvbmx5IGJ5IGhpcyByZWFzb24sIGJ1dCBieSB0aGlzIHNpbmd1bGFyIHBhc3Npb24gZnJvbSBvdGhlciBhbmltYWxzLCB3aGljaCBpcyBhIGx1c3Qgb2YgdGhlIG1pbmQsIHRoYXQgYnkgYSBwZXJzZXZlcmFuY2Ugb2YgZGVsaWdodCBpbiB0aGUgY29udGludWVkIGFuZCBpbmRlZmF0aWdhYmxlIGdlbmVyYXRpb24gb2Yga25vd2xlZGdlLCBleGNlZWRzIHRoZSBzaG9ydCB2ZWhlbWVuY2Ugb2YgYW55IGNhcm5hbCBwbGVhc3VyZS4=";
        const trueTest = "01010100 01001000 01000101 00100000 01010001";

        cipher = new BinaryCode();
        CipherTester.testIdentify(cipher, trueTest);
        CipherTester.testIdentify_Negative(cipher, falseTest);
    });
    describe("when calling method apply", () => {
        const unencryptedPhrase = "THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG";
        const encryptedPhrase = "01010100 01001000 01000101 00100000 01010001 01010101 01001001 01000011 01001011 00100000 01000010 01010010 01001111 01010111 01001110 00100000 01000110 01001111 01011000 00100000 01001010 01010101 01001101 01010000 01010011 00100000 01001111 01010110 01000101 01010010 00100000 01010100 01001000 01000101 00100000 01001100 01000001 01011010 01011001 00100000 01000100 01001111 01000111";
        const encryptedConcatenatedPhrase = "01010100010010000100010100100000010100010101010101001001010000110100101100100000010000100101001001001111010101110100111000100000010001100100111101011000001000000100101001010101010011010101000001010011001000000100111101010110010001010101001000100000010101000100100001000101001000000100110001000001010110100101100100100000010001000100111101000111";

        cipher = new BinaryCode();
        CipherTester.testApply(cipher, encryptedPhrase, unencryptedPhrase);
        CipherTester.testApply(cipher, encryptedConcatenatedPhrase, unencryptedPhrase);
    });
});
