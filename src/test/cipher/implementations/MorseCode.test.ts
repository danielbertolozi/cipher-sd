import { MorseCode } from "../../../main/cipher/implementations/MorseCode";
import { CipherTester } from "./CipherTester";

let cipher: MorseCode;
context("MorseCode", () => {
    describe("when calling function checkIfAppliable", () => {
        const falseTest = "hello world";
        const trueTest = ".... . .-.. .-.. --- / .-- --- .-. .-.. -..";

        cipher = new MorseCode();
        CipherTester.testIdentify(cipher, trueTest);
        CipherTester.testIdentify_Negative(cipher, falseTest);
    });
    describe("when calling method apply", () => {
        const unencryptedPhrase = "the quick brown fox jumps over the lazy dog";
        const encryptedPhrase = "- .... . / --.- ..- .. -.-. -.- / -... .-. --- .-- -. / ..-. --- -..- / .--- ..- -- .--. ... / --- ...- . .-. / - .... . / .-.. .- --.. -.-- / -.. --- --.";

        cipher = new MorseCode();
        CipherTester.testApply(cipher, encryptedPhrase, unencryptedPhrase);
    });
});
