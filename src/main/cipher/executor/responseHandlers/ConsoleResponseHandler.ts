import { CipherResponseHandler } from "./CipherResponseHandler";

export class ConsoleResponseHandler implements CipherResponseHandler {
    forward(response: string): void {
        console.log(response);
    }
}