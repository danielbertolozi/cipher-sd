export interface CipherResponseHandler {
    forward(response: string): void;
}