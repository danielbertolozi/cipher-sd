import { Cipher } from "../Cipher";

export class EligibilityChecker {
    private ciphers: Cipher[];

    constructor(cipherInstances: Cipher[]) {
        this.ciphers = cipherInstances;
    }

    public getEligibleCiphers(input: string): Cipher[] {
        const eligibleCiphers: Cipher[] = [];
        this.ciphers.forEach((cipher) => {
            if (cipher.checkIfAppliable(input)) {
                eligibleCiphers.push(cipher);
            }
        });

        return eligibleCiphers;
    }
}