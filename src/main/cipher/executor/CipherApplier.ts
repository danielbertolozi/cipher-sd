import { Cipher } from "../Cipher";
import { CipherResponseHandler } from "./responseHandlers/CipherResponseHandler";

export class CipherApplier {
    private ciphers: Cipher[];
    private responseHandler: CipherResponseHandler;

    constructor(cipherProvider: Cipher[]) {
        this.ciphers = cipherProvider;
    }

    public setResponseHandler(handler: CipherResponseHandler): CipherApplier {
        this.responseHandler = handler;
        return this;
    }

    public apply(input: string): void {
        this.ciphers.forEach((cipher) => {
            const result = cipher.apply(input);
            if (this.responseHandlerHasNotBeenSet()) {
                throw new Error("You provide a Response Handler for this Cipher Applier");
            }
            this.responseHandler.forward(result);
        });
    }

    private responseHandlerHasNotBeenSet(): boolean {
        return this.responseHandler === undefined;
    }
}