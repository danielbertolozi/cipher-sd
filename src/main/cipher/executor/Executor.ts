import { EligibilityChecker } from "./EligibilityChecker";
import { CipherApplier } from "./CipherApplier";
import CipherInstanceExporter from "../CipherInstanceExporter";
import { ConsoleResponseHandler } from "./responseHandlers/ConsoleResponseHandler";

export class Executor {
    private eligibilityChecker: EligibilityChecker;

    constructor() {
        this.eligibilityChecker = new EligibilityChecker(CipherInstanceExporter);
    }

    public execute(input: string): void {
        const eligibleCiphers = this.eligibilityChecker.getEligibleCiphers(input);
        const cipherApplier = new CipherApplier(eligibleCiphers)
            .setResponseHandler(new ConsoleResponseHandler());
        cipherApplier.apply(input);
    }
}