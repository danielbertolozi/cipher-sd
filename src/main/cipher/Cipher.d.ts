export interface Cipher {
    checkIfAppliable(input: string): boolean;
    apply(input: string): string;
}