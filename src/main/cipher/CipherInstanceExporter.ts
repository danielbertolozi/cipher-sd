import { BaconianCipher } from "./implementations/BaconianCipher";
import { B64 } from "./implementations/B64";
import { BinaryCode } from "./implementations/BinaryCode";
import { CesarCipher } from "./implementations/CesarCipher";
import { MorseCode } from "./implementations/MorseCode";

export default [
    new BaconianCipher(),
    new B64(),
    new BinaryCode(),
    new CesarCipher(),
    new MorseCode()
]