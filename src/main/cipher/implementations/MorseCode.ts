import { BaseCipher } from "./BaseCipher";

export class MorseCode extends BaseCipher {
    constructor() {
        super();
        this.matchingRegex = "^[/ \.\s\-]+$";
    }

    public apply(input: string): string {
        let output: string = "";
        const words: string[] = input.split(" ");
        words.forEach((word: string) => {
            output += this.convertCharIntoMorse(word.trim());
        });
        return output;
    }

    private convertCharIntoMorse(letter: string): string {
        return MorseToAlphabet[letter] ? MorseToAlphabet[letter] : "?";
    }
}

const MorseToAlphabet: { [key: string]: string} = {
    ".-": "a",
    "-...": "b",
    "-.-.": "c",
    "-..": "d",
    ".": "e",
    "..-.": "f",
    "--.": "g",
    "....": "h",
    "..": "i",
    ".---": "j",
    "-.-": "k",
    ".-..": "l",
    "--": "m",
    "-.": "n",
    "---": "o",
    ".--.": "p",
    "--.-": "q",
    ".-.": "r",
    "...": "s",
    "-": "t",
    "..-": "u",
    "...-": "v",
    ".--": "w",
    "-..-": "x",
    "-.--": "y",
    "--..": "z",
    ".----": "1",
    "..---": "2",
    "...--": "3",
    "....-": "4",
    ".....": "5",
    "-....": "6",
    "--...": "7",
    "---..": "8",
    "----.": "9",
    "-----": "0",
    "/": " ",
    ".-.-.-": ".",
    "--..--": ",",
    "---...": ":",
    "..--..": "?",
    ".----.": "'",
    "-....-": "-",
    "-..-.": "/",
    ".--.-.": "@",
    "-...-": "=",
};