import { BaseCipher } from "./BaseCipher";

export class BaconianCipher extends BaseCipher {
    constructor() {
        super();
        this.matchingRegex = "^[AB ab]+$";
    }

    public apply(input: string): string {
        const arrayOfWords = this.breakInputIntoArraysOfWords(input);
        let output: string = "";
        arrayOfWords.forEach((word: string[]) => {
            word.forEach((baconianLetter) => {
                output += this.getLetterFromDictionary(baconianLetter);
            });
            output += " ";
        });
        return output.trimRight();
    }

    private breakInputIntoArraysOfWords(input: string): string[][] {
        const arrayOfWords = input.toUpperCase().split(" ");
        let output: string[][] = [];
        arrayOfWords.forEach((word: string) => {
            const arrayOfLettersForEachWord = this.breakWordIntoArrayWithSeparateLetters(word);
            output.push(arrayOfLettersForEachWord);
        });
        return output;
    }

    private breakWordIntoArrayWithSeparateLetters(word: string): string[] {
        const arrayOfLettersForEachWord: string[] = [];
        while (word.length > 4) {
            arrayOfLettersForEachWord.push(word.substr(0, 5));
            word = word.substr(5, word.length);
        }
        return arrayOfLettersForEachWord;
    }

    private getLetterFromDictionary(baconianLetter: string): string {
        return BaconianCipherDictionary[baconianLetter] ? BaconianCipherDictionary[baconianLetter] : baconianLetter
    }
}

const BaconianCipherDictionary: {[key: string]: string} = {
    "AAAAA": "A",
    "AAAAB": "B",
    "AAABA": "C",
    "AAABB": "D",
    "AABAA": "E",
    "AABAB": "F",
    "AABBA": "G",
    "AABBB": "H",
    "ABAAA": "I",
    "ABAAB": "J",
    "ABABA": "K",
    "ABABB": "L",
    "ABBAA": "M",
    "ABBAB": "N",
    "ABBBA": "O",
    "ABBBB": "P",
    "BAAAA": "Q",
    "BAAAB": "R",
    "BAABA": "S",
    "BAABB": "T",
    "BABAA": "U",
    "BABAB": "V",
    "BABBA": "W",
    "BABBB": "X",
    "BBAAA": "Y",
    "BBAAB": "Z"
}