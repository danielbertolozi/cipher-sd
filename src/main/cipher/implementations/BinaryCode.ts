import { BaseCipher } from "./BaseCipher";

export class BinaryCode extends BaseCipher {
    constructor() {
        super();
        this.matchingRegex = "^[0 1]+$";
    }

    apply(input: string): string {
        let result = "";
        let arr = input.split(" ");
        if (arr.length === 1) {
            arr = separateStringIntoBytes(arr);
        }
        result = performConversionOnByteSeparatedValues(arr);
        return result;
    }
}

function separateStringIntoBytes(arr: string[]) {
    let binaryValue = arr[0];
    const byteArray: string[] = [];
    while (binaryValue.length >= 8) {
        const byte = binaryValue.substr(0, 8);
        byteArray.push(byte);
        binaryValue = binaryValue.substr(8);
    }
    return byteArray;
}

function performConversionOnByteSeparatedValues(arr: string[]) {
    let result = "";
    for (let i = 0; i < arr.length; i++) {
        // @ts-ignore as fromCharCode does work with strings
        result += String.fromCharCode(parseInt(arr[i], 2).toString(10));
    }
    return result;
}