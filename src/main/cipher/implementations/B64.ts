import { BaseCipher } from "./BaseCipher";

export class B64 extends BaseCipher {
    constructor() {
        super();
        this.matchingRegex = "^(?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=)?$";
    }

    public checkIfAppliable(input: string): boolean {
        return input.replace("\n", "").match(this.matchingRegex) != null;
    }    
    
    public apply(input: string): string {
        return Buffer.from(input, "base64").toString();
    }
}
