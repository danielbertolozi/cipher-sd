import { Cipher } from "../Cipher";

export abstract class BaseCipher implements Cipher {
    protected matchingRegex: string;

    public checkIfAppliable(input: string): boolean {
        return input.match(this.matchingRegex) != null;
    }
    abstract apply(input: string): string;
}