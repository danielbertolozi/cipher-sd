import { BaseCipher } from "./BaseCipher";

export class CesarCipher extends BaseCipher {
    constructor() {
        super();
    }
    
    apply(input: string): string {
        const results: string[] = [];
        for (let i = 0; i <= 26; i++) { // using the complete alphabet here
            results.push(performCesarShift(input, i));
        }
        return results.join("\n");
    }

    // Overriding because cesar cipher doesn't use regex
    public checkIfAppliable(input: string): boolean {
        const charArray = input.split("");
        const uniqueCharCount = new Set(charArray).size;
        if (uniqueCharCount >= 3) {
            return true;
        }
        return false;
    }
}

function performCesarShift(input: string, key: number): string {
    let output: string = "";
    if (key < 0) {
        // perform reverse cesar shift
        return performCesarShift(input, key + 26);
    }
    for (let charIndex = 0; charIndex < input.length; charIndex++) {
        let char = input[charIndex];
        if (isALetter(char)) {
            const charCode = input.charCodeAt(charIndex);
            if (charIsUpperCase(charCode)) {
                char = shiftUpperCaseChar(charCode, key);
            } else if (charIsLowerCase(charCode)) {
                char = shiftLowerCaseChar(charCode, key);
            }
        }
        output += char;
    }
    return output;
}

function isALetter(char: string) {
    return char.match("[a-zA-Z]") != null;
}

function charIsUpperCase(charCode: number) {
    return (charCode >= 65) && (charCode <= 90);
}

function charIsLowerCase(charCode: number) {
    return (charCode >= 97) && (charCode <= 122);
}

function shiftUpperCaseChar(charCode: number, key: number): string {
    return String.fromCharCode(((charCode - 65 + key) % 26) + 65);
}

function shiftLowerCaseChar(charCode: number, key: number) {
    return String.fromCharCode(((charCode - 97 + key) % 26) + 97);
}