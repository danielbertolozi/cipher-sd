import { ArgumentHandler } from "./ArgumentHandler";

export class CLIInterface {
    private argumentHandler: ArgumentHandler;
    constructor(argumentHandler: ArgumentHandler) {
        this.argumentHandler = argumentHandler;
    }

    public main(args: string[]): void {
        this.argumentHandler.callUnderlyingHandler(args);
    }
}