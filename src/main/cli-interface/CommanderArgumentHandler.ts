import * as commander from "commander";
import { ArgumentHandler } from "./ArgumentHandler";
import { Executor } from "../cipher/executor/Executor";

export class CommanderArgumentHandler implements ArgumentHandler {
    public callUnderlyingHandler(args: string[]): void {
        this.printCommandLineOptions();
        this.setUpCommands();
        this.run(args);
    }

    private printCommandLineOptions(): void {
        commander
            .version("0.1")
            .on("--help", this.displayHelp);
    }

    private displayHelp(): void {
        console.log("Usage: \n\nPaste the phrase you want to decode here after the program name in order to try to decode it.");
    }

    private run(args: string[]): void {
        commander.parse(args);
    }

    private setUpCommands(): void {
        commander
            .command("*")
            .action((arg: string) => {
                if (arg) {
                    this.executeSearchAndDestroyOnInput(arg);
                }
            })
    }

    private executeSearchAndDestroyOnInput(arg: string) {
        const executor = new Executor();
        executor.execute(arg);
    }
}