export interface ArgumentHandler {
    callUnderlyingHandler(args: string[]): void;
}